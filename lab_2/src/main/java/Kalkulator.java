import java.util.Scanner;

/**
 * Created by tirta.akdi on 15/09/2018.
 */
public class Kalkulator {
    static int angka1, angka2, hasil;
    static String operator;



    public static int getAngka1() {
        return angka1;
    }

    public void setAngka1(int angka1) {
        this.angka1 = angka1;
    }

    public static int getAngka2() {
        return angka2;
    }

    public void setAngka2(int angka2) {
        this.angka2 = angka2;
    }

    public static String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public static int hitung(int angka1, String operator, int angka2){
        int hasil=0;
        if(operator.equals("+")){
            hasil = angka1+angka2;
        }else if(operator.equals("-")){
            hasil = angka1-angka2;
        }else if(operator.equals("*")){
            hasil = angka1*angka2;
        }else if(operator.equals("/")){
            hasil = angka1/angka2;
        }else if(operator.equals("%")){
            hasil = angka1%angka2;
        }else if(operator.equals("^")){
            double an1 = (double)angka1;
            double an2 = (double)angka2;
            int pangkat = (int) Math.pow(an1, an2);
            hasil = pangkat;
        }else if(operator.equals("v")){
            if(angka2 == 2){
                double an1 = (double)angka1;
                int akadrat = (int) Math.sqrt(an1);
                hasil = akadrat;
            }else if(angka2 == 3){
                double an1 = (double)angka1;
                int akabik = (int) Math.cbrt(an1);
                hasil = akabik;
            }
        }else {
            System.out.println("Operator tidak dikenali");
        }
        return hasil;
    }
    public static void main(String arg[]) {
        int a1, a2;
        String op;
        Scanner in = new Scanner(System.in);

        System.out.println("KALKULATOR SYANTIK");
        System.out.println("------------------");
        System.out.print("Masukkan Input : ");
        a1 = in.nextInt();
        op = in.next();
        a2 = in.nextInt();

        Kalkulator kal = new Kalkulator();
        System.out.println("Hasil Perhitungan : "+kal.hitung(a1, op, a2));
    }
}
