// membuat sebuah class bernama kucing
public class Kucing {
	// membuat  variabel untuk menampung attribut atau sifat dari kucing
    String nama = "Royya";
	String ras;
	int umur=0;
	
public Kucing(String nama, String ras, int umur){
	this.nama = nama;
	this.ras = ras;
	this.umur = umur;
}
	
	public String getNama(){
		return nama;
	}
	
	public String getJenis(){
		return ras;
	}
	
	public int getUmur(){
		return umur;
	}
	
	public void setNama(String nama){
		this.nama = nama;
	}
	
	public void setJenis(String ras){
		this.ras = ras;
	}
	
	public void setUmur(int umur){
		this.umur = umur;
	}
    
	// membuat fungsi
    public String mengeong() {
		String suara = "Meooooooonnnnnng";
        return suara;
    }
	
	public String berjalan(int jmlLangkah) {
        String langkah = "Kucingku, "+this.nama+" telah berjalan sejauh "+jmlLangkah+" langkah";
		return langkah;
    } 
	
	public String tidur() {
        umur+=1;
		String umurr = "Umur kucingku sekarang bertambah satu tahun, total umur dia sekarang adalah "+umur;
		return umurr;
    } 
    
    public static void main(String[] args) {
      Kucing cat = new Kucing("Royya", "Persia", 23);
	  System.out.println(cat.mengeong());
	  System.out.println(cat.berjalan(23));
	  System.out.println(cat.tidur());
    }
}