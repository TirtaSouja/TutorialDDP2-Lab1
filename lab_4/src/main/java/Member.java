public class Member {
    private int id, umur, saldo, nMember;
    private String nama;
    private boolean memStatus;

    public Member(int id, String nama, int umur, int saldo) {
        this.id = id;
        this.nama = nama;
        this.umur = umur;
        this.saldo = saldo;
        this.nMember = id;
        this.memStatus = false;
    }

    public Member(int id, String nama, int umur) {
        this.id = id;
        this.nama = nama;
        this.umur = umur;
        this.saldo = 50000;
        this.nMember = id;
        this.memStatus = false;
    }    

    public String getNama() {
        return nama;
    }

    public int getSaldo() {
        return saldo;
    }

    public int getUmur() {
        return umur;
    }

    public int getNomorMember() {
        return id;
    }

    public boolean isMemberTutup() {
        return memStatus;
    }
    
    public String toString() {
        if (umur < 50) {
            return "Nomor Member	: " + getNomorMember()
                    + "\nNama		: " + getNama()
                    + "\nUmur		: " + getUmur()
                    + "\nSaldo		: " + getSaldo();
        } else {
            return "Member atas nama " + getNama()
                    + " telah ditutup";
        }
    }

    public void kirimSaldo(Member B, int saldo) {
        if (getNama().equals(B.getNama())) {
            System.out.println("Transaksi gagal!\nTidak bisa melakukan transfer ke akun sendiri!");
        } else if (isMemberTutup()== true) {
            System.out.println("Transaksi gagal!\nMember atas nama " + getNama() + " telah ditutup");
        } else if (B.isMemberTutup()== true) {
            System.out.println("Transaksi gagal!\nMember atas nama " + B.getNama() + " telah ditutup");
        } else {
            this.saldo -= saldo;
            B.saldo += saldo;
            String text = getNama() + " telah berhasil mengirim saldo ke " 
                    + B.getNama() + " sebesar " + saldo+"\nSaldo " + getNama()
                    + " saat ini " + getSaldo()+"\nSaldo " + B.getNama()
                    + " saat ini " + B.getSaldo();
            System.out.println(text);
        }
    }

    public void topUpSaldo(int saldo) {
        if (isMemberTutup()== true) {
            System.out.println("Member atas nama " + getNama() + " telah ditutup");
        } else {
            this.saldo += saldo;
            System.out.println(getNama() + " telah berhasil top up saldo"
                    +"\nSaldo " + getNama() + " saat ini " + getSaldo());
        }
    }

    public void tutupMember() {
        this.umur += 50;
        this.memStatus = true;
        this.nMember -= 1;
        System.out.println("Member atas nama " + getNama() + " telah resmi ditutup");
    }

    public void setUmur(int umur) {
        this.umur += umur;
        if (this.umur < 50) {
            System.out.println("umur "+getNama()+" menjadi "+getUmur());
        } else {
            this.nMember -= 1;
            this.memStatus = true;
            System.out.println(getNama()+" terlalu tua untuk menjadi member"
            +"\nMember atas nama " + getNama() + " telah resmi ditutup");
        }
    }
}
