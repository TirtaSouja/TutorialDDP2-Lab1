import java.util.Scanner;

public class PesanRahasia {

    /**
     *
     * @param iniPesan merupakan satu baris pesan rahasia
     * @return kalimat X
     */

    /* fungsi pertama yang di jalankan
        dalam fungsi ini parameter String "iniPesan" telah membawa nilai inputan user
    */
    public static String constructX(String iniPesan){
        // membuat variabel kosong bertipe string untuk menampung isi pesan setelah di lakukan pemilihan
        String kalimat="";
        // untuk melakukan pemilihan digunakan fungsi iteration for
        for(int i=0; i<iniPesan.length(); i++ ){
            // dilakukan pengecekkan untuk huruf atau karakter yang berindex genap
            if(i%2 == 0){
                // setiap karakter di index genap akan dimasukkan ke variabel kalimat
                kalimat += iniPesan.charAt(i);
            }
        }
        // mengembalikan nilai yang telah di dapat dalam variabel kalimat (melempar ke fungsi constructY)
        return kalimat;
    }

    /**
     *
     * @param kalimatX hasil dari constructX
     * @return kalimat Y
     */
     
     /*
        fungsi di bawah ini adalah fungsi constructY dengan parameter kalimatX
     */
    public static String constructY(String kalimatX) {
        // membuat variabel kosong lauinnya dengan tipe String
        String kalimatY="";
        // membuat variabel kosong bertipe integer untuk menampung jumlah huruf 'A' yang di temukan
        int a=0;
        // melakukan perulangan selama jumlah huruf 'A' masih belum sebanyak 3 buah
        for(int i=0; a<3; i++ ){
            // memasukkan setiap karakter kedalam variabel kalimatY selama jumlah karakter 'A' yang ditemukan belum 3 buah
            kalimatY += kalimatX.charAt(i);
            // pengecekkan untuk setiap karakter yang di masukkan, apakah sama dengan 'A' atau tidak
            if(kalimatX.charAt(i) == 'A'){
                // jika karakter 'A' ditemukan maka nilai variabel a akan bertambah satu
                a+=1;
            }
        }
        // mengembalikan nilai yang tersimpan di kalimatY (dilempar ke fungsi constructIsiPesanSebenarnya)
        return kalimatY;
    }

    /**
     *
     * @param kalimatY hasil dari construct Y
     * @return hasil terjemahan pesan
     */
     
     /*
        fungsi di bawah adalah fungsi untuk melihat pesan rahasia dari inputan user di awal tadi
     */
    public static String constructIsiPesanSebenarnya(String kalimatY) {
        // membuat sebuah variabel kosong bertipe string
        String kalimatF="";
        // melakukan perulangan untuk memilah pesan
        for(int i=0; i<kalimatY.length(); i++ ){
            // pemilihan untuk menyaring pesans ebenarnya dengan mengambil karakter di index ganjil saja
            if(i%2 == 1){
                // memasukkan setiap karakter yang ditemukan kedalam variabel kalimatF
                kalimatF += kalimatY.charAt(i);
            }
        }
        // mengembalikan kalimatF yang berisikan pesan sebenarnya untuk di munculkan sebagai output ke user
        return kalimatF;
    }

    public static void main(String[] args) {
        // membuat object scanner untuk menerima inputan user
        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukkan Pesan      : ");
        // menampung inputan user di variabel pesan bertipe string
        String pesan = scanner.nextLine();
        // menampilkan isi pesan rahasia dengan memanggil fungsi constructIsiPesanSebenarnya(constructY(constructX(pesan)))
        System.out.printf("Isi Sebenarnya      : %s\n", constructIsiPesanSebenarnya(constructY(constructX(pesan))));
        scanner.close();
    }
}
